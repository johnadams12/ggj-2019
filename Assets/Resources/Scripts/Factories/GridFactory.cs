﻿using System;
using System.Collections.Generic;
using Assets.Resources.Scripts.Map.data;
using UnityEngine;

namespace Assets.Resources.Scripts.Factories
{
    public class GridFactory
    {
        public static double MAP_WIDTH = 90;
        public static double MAP_HEIGHT = 55;

        public static double ROAD_WIDTH = 10.5;
        public static double VROAD_WIDTH = 4.5;

        public static double BLOCK_HEIGHT = 25;
        public static double BLOCK_WIDTH = 25;

        public CityGrid Generate()
        {
            List<double> verticals = new List<double>();
            List<double> horizontals = new List<double>();

            double j = 0;
            int i;
            for (i = 0; j < MAP_HEIGHT; ++i)
            {
                if ((i % 2) == 0)
                {
                    horizontals.Add(j);
                    j += ROAD_WIDTH;
                }
                else
                {
                    j += BLOCK_HEIGHT;
                }
            }

            j = 0;
            for (i = 0; j < MAP_WIDTH; ++i)
            {
                if ((i % 2) == 0)
                {
                    verticals.Add(j);
                    j += VROAD_WIDTH;
                }
                else
                {
                    j += BLOCK_WIDTH;
                }
            }

            return new CityGrid
            {
                Verticals = verticals.ToArray(),
                Horisontals = horizontals.ToArray(),
            };
        }
    }
}