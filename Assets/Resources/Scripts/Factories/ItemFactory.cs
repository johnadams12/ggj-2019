﻿using System.Collections.Generic;
using UnityEngine;

public class ItemFactory : MonoBehaviour
{
    private Dictionary<string, GameObject> prefabs = new Dictionary<string, GameObject>();
    private Dictionary<string, Sprite> sprites = new Dictionary<string, Sprite>();
    private Dictionary<string, Texture2D> textures = new Dictionary<string, Texture2D>();

    public void Init()
    {
        Constants.PREFAB_NAMES.ForEach(prefabName => prefabs.Add(prefabName, LoadPrefab(prefabName)));
    }

    private GameObject LoadPrefab(string prefabName)
    {
        return (GameObject) Resources.Load(Constants.PREFAB_PATH + prefabName);
    }

    public Sprite LoadSprite(string spriteName)
    {
        if (sprites.ContainsKey(spriteName))
        {
            return sprites[spriteName];
        }

        sprites[spriteName] = Resources.Load<Sprite>(Constants.IMAGE_PATH + "Buildings/" + spriteName);
        return sprites[spriteName];
    }

    
    public Texture2D LoadTexture(string textureName)
    {
        if (textures.ContainsKey(textureName))
        {
            return textures[textureName];
        }

        textures[textureName] = Resources.Load<Texture2D>(Constants.IMAGE_PATH + "Buildings/" + textureName);
        return textures[textureName];
    }

    public GameObject CreateItem(string type, float x, float y)
    {
        return CreateFromPrefab(type, x, y);
    }

    private GameObject CreateFromPrefab(string prefabName, float x, float y)
    {
        if (prefabs.ContainsKey(prefabName))
        {
            var instance = Instantiate(prefabs[prefabName], new Vector3(x, y, -1), Quaternion.identity);
            AppendToParent(instance);
            return instance;
        }

        return null;
    }

    private void AppendToParent(GameObject instance)
    {
        instance.transform.SetParent(GetComponent<MapManager>().transform);
    }
}