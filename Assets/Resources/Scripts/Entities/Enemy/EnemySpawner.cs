﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;

public class EnemySpawner : MonoBehaviour
{
    public int index;
    public bool HasClue = false;
    private ItemFactory itemFactory;

    public GameObject fog;
    private int AliveCount;

    private List<EnemyController> Enemies = new List<EnemyController>();

    private MapManager mapManager;

    // Use this for initialization
    private void Start()
    {
        itemFactory = FindObjectOfType<ItemFactory>();
        mapManager = FindObjectOfType<MapManager>();
    }

    public void SpawnEnemies(List<int> indexes)
    {
        AliveCount = Constants.ENEMIES_PER_SPAWN;
        if (itemFactory == null)
        {
            itemFactory = FindObjectOfType<ItemFactory>();
        }

        for (var i = 0; i < Constants.ENEMIES_PER_SPAWN; i++)
        {
            var item = itemFactory.CreateItem(Random.value < 0.5 ? Constants.ENEMY_01 : Constants.ENEMY_02,
                transform.position.x, transform.position.y);
            item.GetComponent<EnemyController>().enemySpawner = this;
            item.GetComponent<EnemyController>().ShowClue = indexes.Contains(this.index);
            if (indexes.Contains(this.index))
            {
                print("Clue"+this.index);
            }
            Enemies.Add(item.GetComponent<EnemyController>());
        }
    }

    public List<EnemyController> GetEnemiesInRange(Vector3 playerPosition, float range)
    {
        var result = new List<EnemyController>();

        foreach (var enemyController in Enemies)
        {
            if (enemyController == null) continue;

            float distance = Vector2.Distance(enemyController.transform.position, playerPosition);
            if (distance <= range)
            {
                result.Add(enemyController);
            }
        }

        return result;
    }

    public void UpdateRelated()
    {
        AliveCount -= 1;
        if (AliveCount == 0)
        {
            //fog.SetActive(false);
            StartCoroutine(FadeOut(fog.gameObject.GetComponent<SpriteRenderer>()));

            AudioSourceManager.instance.Play(AudioResources.OPEN_DISTRICT);
        }
    }

    private YieldInstruction fadeInstruction = new YieldInstruction();

    IEnumerator FadeOut(SpriteRenderer image)
    {
        float elapsedTime = 0.0f;
        Color c = image.color;
        while (elapsedTime < 3f)
        {
            yield return fadeInstruction;
            elapsedTime += Time.deltaTime;
            c.a = 1.0f - Mathf.Clamp01(elapsedTime / 3f);
            image.color = c;
        }

        fog.SetActive(false);
    }

    // Update is called once per frame
    private void Update()
    {
    }
}