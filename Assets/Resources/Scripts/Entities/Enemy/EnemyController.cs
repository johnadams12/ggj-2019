﻿using System.Collections;
using UnityEngine;

public class EnemyStates
{
    public static readonly string ENEMY_IDLE = "Idle";
    public static readonly string ENEMY_ATTACK = "EnemyAttacking";
    public static readonly string ENEMY_WALKING = "EnemyWalking";
    public static readonly string ENEMY_DEAD = "EnemyDead";
}

public class EnemyController : MonoBehaviour
{
    public EnemySpawner enemySpawner;
    public bool ShowClue;
    private PlayerController playerController;
    private Rigidbody2D rigidbody2D;
    private Animator animator;
    private SpriteRenderer spriteRenderer;
    private MapManager mapManager;
    private string State = EnemyStates.ENEMY_IDLE;
    private float AttackTime;
    private ItemFactory itemFactory;
    private float elapsed = 0f;

    private int Health = Constants.ENEMY_HEALTH;

    // Use this for initialization
    private void Start()
    {
        itemFactory = FindObjectOfType<ItemFactory>();
        mapManager = FindObjectOfType<MapManager>();
        playerController = FindObjectOfType<PlayerController>();
        animator = gameObject.GetComponent<Animator>();
        spriteRenderer = gameObject.GetComponent<SpriteRenderer>();
        rigidbody2D = gameObject.GetComponent<Rigidbody2D>();
    }

    // Update is called once per frame
    private void FixedUpdate()
    {
        if (Health <= 0) return;
        if (mapManager.isInMapMode()) return;
        float dist = Vector3.Distance(playerController.transform.position, transform.position);
        if (dist <= Constants.ENEMY_VISION_RANGE)
        {
            State = EnemyStates.ENEMY_WALKING;
        }

        if (dist <= Constants.ENEMY_ATACK_RANGE)
        {
            State = EnemyStates.ENEMY_ATTACK;
        }

        animator.SetBool(EnemyStates.ENEMY_WALKING, State.Equals(EnemyStates.ENEMY_WALKING));
        if (State.Equals(EnemyStates.ENEMY_WALKING))
        {
            animator.SetBool(EnemyStates.ENEMY_ATTACK, false);
            var start = gameObject.transform.position;
            var end = new Vector2(playerController.transform.position.x, playerController.transform.position.y);
            var step = Constants.ENEMY_SPEED * Time.deltaTime;
            gameObject.transform.position = Vector3.MoveTowards(start, end, step);
            int direction = start.x >= end.x ? 1 : -1;
            transform.localScale = new Vector2(Mathf.Abs(transform.localScale.x) * direction,
                transform.localScale.y);
        }

        if (playerController.transform.position.y < transform.position.y)
        {
            spriteRenderer.sortingOrder = 90;
        }
        else
        {
            spriteRenderer.sortingOrder = 100;
        }

        RepeatEachSecond();
    }

    private void RepeatEachSecond()
    {
        elapsed += Time.deltaTime;
        if (elapsed >= 1f)
            if (State.Equals(EnemyStates.ENEMY_ATTACK))
            {
                animator.SetBool(EnemyStates.ENEMY_ATTACK, true);
                elapsed = elapsed % 1f;
                playerController.ApplyDamage(Constants.ENEMY_DAMAGE);
            }
    }

    public void ApplyDamage(int damage)
    {
        Health -= damage;
        print("Enemy health: " + Health);
        if (Health <= 0)
        {
            enemySpawner.UpdateRelated();
            State = EnemyStates.ENEMY_DEAD;
            GetComponent<Collider2D>().enabled = false;
            spriteRenderer.sortingOrder = 10;
            animator.SetBool(EnemyStates.ENEMY_DEAD, true);

            mapManager.TutorialTextController.OnEnemyDeath();
            StartCoroutine("StartDeathRoutine");
        }
    }

    private IEnumerator StartDeathRoutine()
    {
        yield return new WaitForSeconds(2f);

        //create for first enemy
        if (enemySpawner.HasClue || ShowClue)
        {
            var start = gameObject.transform.position;
            var end = new Vector2(playerController.transform.position.x, playerController.transform.position.y);
            int direction = start.x >= end.x ? 1 : -1;
            var clue = itemFactory.CreateItem(Constants.Q_MARK, transform.position.x+direction*1f, transform.position.y);
            clue.tag = "Clue";
        }

        Destroy(gameObject);
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Player") State = EnemyStates.ENEMY_ATTACK;
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Player") State = EnemyStates.ENEMY_WALKING;
    }
}