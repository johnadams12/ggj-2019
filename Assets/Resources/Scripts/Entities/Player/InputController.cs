﻿using UnityEngine;

public class InputController : MonoBehaviour
{
    private PlayerController playerController;
    private MapManager mapManager;

    void Start()
    {
        playerController = gameObject.GetComponent<PlayerController>();
        mapManager = GameObject.FindObjectOfType<MapManager>();
    }

    void FixedUpdate()
    {
        float moveHorizontal = Input.GetAxisRaw("Horizontal");
        float moveVertical = Input.GetAxisRaw("Vertical");
        playerController.MovePlayer(new Vector2(moveHorizontal, moveVertical));
        if (Input.GetButtonDown("Fire1"))
        {
            print("Fire1");
            playerController.PlayerAttack();
        }

        if (Input.GetKeyDown(KeyCode.Escape))
        {
            mapManager.SwitchMap();
        }
    }
}