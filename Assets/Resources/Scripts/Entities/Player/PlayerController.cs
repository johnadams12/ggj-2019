﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class PlayerStates
{
    public static readonly string PLAYER_IDLE = "Idle";
    public static readonly string PLAYER_WALKING = "PlayerWalking";
    public static readonly string PLAYER_ATACKING = "PlayerAttacking";
    public static readonly string PLAYER_DEAD = "PlayerDead";
}

public class PlayerController : MonoBehaviour
{
    private Rigidbody2D rigidbody2D;
    private int health = Constants.PLAYER_HEALTH;
    // private Text healthText;
    private Transform healthBar;
    private bool playerLocked;
    private SpriteRenderer spriteRenderer;
    private Animator animator;
    private MapManager mapManager;

    public AudioSource WalkAudioSource;
    private bool playerDead = false;

    private void Start()
    {
        mapManager = FindObjectOfType<MapManager>();
        rigidbody2D = gameObject.GetComponent<Rigidbody2D>();
        spriteRenderer = gameObject.GetComponent<SpriteRenderer>();
        animator = gameObject.GetComponent<Animator>();
        //healthText = GameObject.FindGameObjectWithTag("HealthText").GetComponent<Text>();
        healthBar = GameObject.FindGameObjectWithTag("HealthBar").transform;
    }

    public void MovePlayer(Vector2 movement)
    {
        if (playerLocked) return;
        if (health <= 0) return;
        rigidbody2D.AddForce(movement * Constants.PLAYER_SPEED);
        FlipPlayer(movement);
        animator.ResetTrigger(PlayerStates.PLAYER_ATACKING);
        animator.SetBool(PlayerStates.PLAYER_WALKING, movement * Constants.PLAYER_SPEED != Vector2.zero);
        if (movement * Constants.PLAYER_SPEED != Vector2.zero)
        {
            if (!WalkAudioSource.isPlaying)
            {
                WalkAudioSource.Play();
            }
        }
        else
        {
            WalkAudioSource.Stop();
        }

    }

    private void FlipPlayer(Vector2 movement)
    {
        if (movement * Constants.PLAYER_SPEED != Vector2.zero && movement.x != 0)
            transform.localScale = new Vector2(Mathf.Abs(transform.localScale.x) * -Mathf.Sign(movement.x),
                transform.localScale.y);
    }

    public void LockPlayer(bool playerLocked)
    {
        this.playerLocked = playerLocked;
        spriteRenderer.color = new Color(1f, playerLocked ? 0.25f : 1f, 1f, 1f);
    }

    public void PlayerAttack()
    {
        if (playerLocked) return;
        animator.SetTrigger(PlayerStates.PLAYER_ATACKING);
        EnemySpawner[] enemySpawners = mapManager.getEnemySpawners();
        bool playHit = false;
        foreach (var enemySpawner in enemySpawners)
        {
            List<EnemyController> enemies =
                enemySpawner.GetEnemiesInRange(transform.position, Constants.PLAYER_ATTACK_RANGE);
            foreach (var enemyController in enemies)
            {
                if (FacingEnemy(enemyController))
                {
                    playHit = true;
                    enemyController.ApplyDamage(Constants.PLAYER_DAMAGE);
                }
            }
        }

        if (playHit)
        {
            AudioSourceManager.instance.Play(AudioResources.PLAYER_ATTACK);
        }
    }

    private bool FacingEnemy(EnemyController enemyController)
    {
        return transform.localScale.x > 0 && enemyController.transform.position.x <= transform.position.x ||
               transform.localScale.x <= 0 && enemyController.transform.position.x >= transform.position.x;
    }

    public void ApplyDamage(int damage)
    {
       // if (playerLocked) return;
        if (health > 0)
        {
            health -= damage;
            //healthText.text = "Health: " + health;
            AudioSourceManager.instance.Play(AudioResources.PLAYER_HITTED);
            drawHealthBar();
        }

        if (health <= 0 && !playerDead)
        {
            playerDead = true;
            animator.SetBool(PlayerStates.PLAYER_DEAD, true);
            StartCoroutine("StartHomeTextRoutine");
        }
    }

    private IEnumerator StartHomeTextRoutine()
    {
        yield return new WaitForSeconds(2f);
        SceneManager.LoadScene("Menu");
    }

    private void drawHealthBar()
    {
        healthBar.localScale = new Vector3(2f * (1f - health / (float) Constants.PLAYER_HEALTH),
            healthBar.localScale.y, healthBar.localScale.z);
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        print(collision.gameObject.tag);
        if (collision.gameObject.tag == "MedKit")
        {
            if (health < 100)
            {
                health += 10;
                AudioSourceManager.instance.Play(AudioResources.HEAL);
                //healthText.text = "Health: " + health;
                drawHealthBar();
                Destroy(collision.gameObject);
            }
        }

        if (collision.gameObject.tag == "Clue")
        {
            mapManager.UpdateClues();
            Destroy(collision.gameObject);
        }
    }
}