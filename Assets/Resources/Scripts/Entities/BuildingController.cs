﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class BuildingController : MonoBehaviour
{
    private static int order = 10;
    private SpriteRenderer spriteRenderer;
    private MapManager mapManager;
    private HouseEntity houseEntity;
    private ItemFactory itemFactory;
    private Shader buildingShader;
    private PlayerController playerController;

    // Use this for initialization
    void Start()
    {
        Init();
    }

    private void Init()
    {
        spriteRenderer = gameObject.GetComponent<SpriteRenderer>();
        mapManager = gameObject.GetComponentInParent<MapManager>();
        itemFactory = GameObject.FindObjectOfType<ItemFactory>();
        playerController = FindObjectOfType<PlayerController>();
    }

    // Update is called once per frame
    void Update()
    {
    }

    public void SetHouseProperties(HouseEntity houseEntity)
    {
        Init();
        this.houseEntity = houseEntity;
        spriteRenderer.sprite = itemFactory.LoadSprite(houseEntity.HouseType + "_base");
        spriteRenderer.sortingOrder = BuildingController.order;
        BuildingController.order += 1;
        gameObject.AddComponent<PolygonCollider2D>();

        var material = new Material(Shader.Find("Sprites/Building"));
        var maskTexture = itemFactory.LoadTexture(houseEntity.HouseType + "_mask");

        material.SetTexture("_Mask", maskTexture);
        material.SetColor("_WindowTint", houseEntity.WindowColor);
        material.SetColor("_WallTint", houseEntity.WallColor);
        material.SetColor("_RoofTint", houseEntity.RoofColor);
        spriteRenderer.material = material;
    }

    void OnMouseOver()
    {
        if (mapManager.isInMapMode())
        {
            print(houseEntity.IsPlayersHouse);
            spriteRenderer.color = new Color(0.8f, 0.5f, 0.5f);
        }
    }

    void OnMouseExit()
    {
        if (mapManager.isInMapMode())
        {
            spriteRenderer.color = new Color(1f, 1f, 1f);
        }
    }

    void OnMouseUp()
    {
        if (mapManager.isInMapMode())
        {
            print("Player selected house " + name);
            print(houseEntity.IsPlayersHouse);
            if (houseEntity.IsPlayersHouse)
            {
                SceneManager.LoadScene("GameEnd");
            }
            else
            {
                playerController.ApplyDamage(Constants.WRONG_HOUSE_DAMAGE);
                mapManager.ShowWrongHouseWarning();
            }
        }
    }
}