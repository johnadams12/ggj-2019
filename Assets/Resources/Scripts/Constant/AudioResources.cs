﻿public class AudioResources
{
    private const string SOUND_PATH = "Sound/";
    public const string AMBIENT_MUSIC = SOUND_PATH + "ambient";
    public const string PLAYER_HITTED = SOUND_PATH + "player_hitted";
    public const string PLAYER_WALKING = SOUND_PATH + "player_walking_ground";
    public const string PLAYER_ATTACK = SOUND_PATH + "head_blown";
    public const string PICK_CLUE = SOUND_PATH + "win";
    public const string OPEN_DISTRICT = SOUND_PATH + "piano";
    public const string HEAL = SOUND_PATH + "heal";

}