﻿using System.Collections.Generic;

public class Constants
{
    public static readonly int CLUE_COUNT = 3;

    public static readonly int WRONG_HOUSE_DAMAGE = 25;

    //PLAYER VALUES
    public static readonly int PLAYER_HEALTH = 100;
    public static readonly float PLAYER_SPEED = 15f;
    public static readonly float PLAYER_ATTACK_RANGE = 1f;
    public static readonly int PLAYER_DAMAGE = 40;

    //ENEMY VALUES
    public static readonly int ENEMIES_PER_SPAWN = 1;
    public static readonly int ENEMY_HEALTH = 100;
    public static readonly int ENEMY_DAMAGE = 10;
    public static readonly float ENEMY_SPEED = 2f;
    public static readonly float ENEMY_VISION_RANGE = 5f;
    public static readonly float ENEMY_ATACK_RANGE = 1f;

    //CAMERA VALUES
    public static readonly float CAMERA_DEFAULT_SIZE = 10f;
    public static readonly float CAMERA_MAP_SIZE = 54f;

    //PATHES
    public static readonly string DEFAULT_LEVEL = "level1";
    public static readonly string IMAGE_PATH = "Images/";
    public static readonly string PREFAB_PATH = "Prefab/";
    public static readonly string LEVELS_PATH = "Levels/";

    //ENTITIES
    public static readonly string BUILDING1 = "Building_01";
    public static readonly string ENEMY_01 = "Enemy_01";
    public static readonly string ENEMY_02 = "Enemy_02";
    public static readonly string Q_MARK = "Q_mark";


    //IMAGES
    public static readonly string BUILDING_BASE1 = "1_base";
    public static readonly string BUILDING_BASE2 = "2_base";
    public static readonly string BUILDING_BASE3 = "3_base";
    public static readonly string BUILDING_BASE4 = "4_base";
    public static readonly string BUILDING_BASE5 = "5_base";
    public static readonly string BUILDING_BASE6 = "6_base";
    public static readonly string BUILDING_BASE7 = "7_base";
    public static readonly string BUILDING_BASE8 = "8_base";


    public static List<string> PREFAB_NAMES = new List<string>()
    {
        BUILDING1, ENEMY_01, ENEMY_02, Q_MARK
    };

    public static List<string> IMAGES_NAMES = new List<string>()
    {
        BUILDING_BASE1, BUILDING_BASE2, BUILDING_BASE3, BUILDING_BASE4, BUILDING_BASE5, BUILDING_BASE6, BUILDING_BASE7,
        BUILDING_BASE8
    };
}