﻿using System.Collections.Generic;
using UnityEngine;

public class AudioSourceManager : MonoBehaviour
{
    public AudioSource EffectsSource;
    public AudioSource MusicSource;
    public static AudioSourceManager instance = null;
    private static Dictionary<string, AudioClip> clips = new Dictionary<string, AudioClip>();

    public static AudioClip loadAudioClip(string resourcePath)
    {
        if (clips.ContainsKey(resourcePath))
        {
            return clips[resourcePath];
        }

        AudioClip audioClip = Resources.Load(resourcePath) as AudioClip;
        clips[resourcePath] = audioClip;
        return audioClip;
    }

    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
        else if (instance != this)
        {
            Destroy(gameObject);
        }
    }

    public void Play(AudioClip clip)
    {
        EffectsSource.PlayOneShot(clip);
    }

    public void Play(string clip_path)
    {
        AudioClip clip = loadAudioClip(clip_path);
        EffectsSource.PlayOneShot(clip);
    }

    public void PlayMusic(AudioClip clip, bool loop = false)
    {
        MusicSource.clip = clip;
        MusicSource.volume = 1f;
        MusicSource.loop = loop;
        MusicSource.Play();
    }

    public void StopMusic()
    {
        MusicSource.Stop();
    }
}