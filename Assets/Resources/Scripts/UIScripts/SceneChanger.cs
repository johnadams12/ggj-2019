﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
enum Scenes {
    TITLE = 0,
    GAME_INTRO = 1,
    MAIN = 2,
    CREDITS = 3,
    MENU = 4,
    GAME_END = 5,
    TUTORIAL_TEXT = 6
}

[RequireComponent(typeof(SceneFader))]
public class SceneChanger : MonoBehaviour
{

    [SerializeField] Scenes sceneToChange;
    [SerializeField] bool blockKeyboardSwitching = false;
    [SerializeField] bool startChangingScene = false;
    [SerializeField] KeyCode keyToSwitch = KeyCode.Return;
    void Update() {
        if (startChangingScene || (Input.GetKeyUp(keyToSwitch) && !blockKeyboardSwitching))
            StartCoroutine(GetComponent<SceneFader>().FadeAndLoadScene(SceneFader.FadeDirection.In,(int)sceneToChange));
    }

}
