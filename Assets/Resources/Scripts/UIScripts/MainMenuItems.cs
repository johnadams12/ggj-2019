﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
[RequireComponent(typeof(SceneFader))]
public class MainMenuItems : MonoBehaviour
{
    [SerializeField] float frequency = 10f;
    [SerializeField] float amp = 2f;
    Text[] items;
    int selectedItem = 0;
    int initialFontSize = -1;
    Dictionary <string, Scenes> TitleToSceneId = new Dictionary<string, Scenes>{
        {"New Game", Scenes.GAME_INTRO},
        {"Credits", Scenes.CREDITS},
    };
    void Start()
    {
        var gameObjects = GameObject.FindGameObjectsWithTag("MenuItem");
        items = new Text[gameObjects.Length];
        var index = 0;
        foreach(var go in gameObjects) {
            var text = go.GetComponent<Text>();
            if (TitleToSceneId.ContainsKey(text.text)) {
                items[index++] = text;
                // hack
                if (text.text == "New Game") {
                    selectedItem = index - 1;
                }
            }
        }

        if (items.Length == 0)
            return;

        initialFontSize = items[0].fontSize;
    }

    void Update()
    {
        if (items.Length == 0)
            return;

        if (Input.GetKeyUp(KeyCode.DownArrow)) 
            selectedItem++;
        if (Input.GetKeyUp(KeyCode.UpArrow)) 
            selectedItem = selectedItem - 1;
        
        if (selectedItem == items.Length)
            selectedItem = 0;
        if (selectedItem < 0)
            selectedItem = items.Length - 1;

        for (var index = 0; index < items.Length; index++)
            if (index == selectedItem) 
                SetupDisplayItem(index, true);
            else 
                SetupDisplayItem(index, false);

        if (Input.GetKeyUp(KeyCode.Return))
        {
            var sceneIndex = TitleToSceneId[items[selectedItem].text];
            StartCoroutine(GetComponent<SceneFader>()
                .FadeAndLoadScene(SceneFader.FadeDirection.In,(int)sceneIndex));
        }
    }

    void SetupDisplayItem(int index, bool setupAsSelected) {
        items[index].fontSize = initialFontSize;

        if (setupAsSelected)
        {
            items[index].fontSize = initialFontSize + 
                (int) (Mathf.Sin(Time.time * frequency) * amp);
        }
        
    }
}
