﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public enum TutorialTexts
{
  MovementTip,
  Hidden,
  Good,
  FightTip,
  ObjectiveTip,
  DeadEnemy,
  EscKey,
  MapView,
GameplayTip,
}

// THIS IS BAD. I'm SORRY
public class TutorialTextController : MonoBehaviour
{
  // Start is called before the first frame update
  Text tutorialText;
  Text textShadow;
  string text { set { textShadow.text = value; tutorialText.text = value; } }
  TutorialTexts currentText = TutorialTexts.MovementTip;
  TutorialTexts nextTutorialText = TutorialTexts.MovementTip;
  float timedTipDelay = 1f;
  bool timedTipFinished = true;
  float currentDelayTime = 1f;
  Dictionary<TutorialTexts, string> tutorials = new Dictionary<TutorialTexts, string>{
      {TutorialTexts.MovementTip, "Press [arrows] or [WASD] to move."},
      {TutorialTexts.Hidden, ""},
      {TutorialTexts.Good, "Great!"},
      {TutorialTexts.FightTip, "Press [Space] or mouse btn to fight"},
      {TutorialTexts.ObjectiveTip, "Our hero is lost. His memory is foggy, he is looking for his home! Explore the city and protect yourself from angry punks."},
      {TutorialTexts.DeadEnemy, "The brave hero survived! And he seems to remember something... After each fight the player MIGHT receive a clue (Q-MARK) about his home. \nPress [esc] key to view the map."},
      {TutorialTexts.EscKey, "Press [esc] key to view the map."},
      {TutorialTexts.MapView, " This is the map of the town where player gets to choose which house matches the clues he collected. Careful! Picking the wrong house will drop 25% of player's HP. \n[LMB] pick\n [ESC] Back"},
      {TutorialTexts.GameplayTip, "You'll win if you pick the right house, however watch out for your heath, collect medkits regularly! Death is the *END*. Good luck"}
  };

  Dictionary<TutorialTexts, int> ShownTutorials = new Dictionary<TutorialTexts, int>();

  public void OnEnemyDeath()
  {
    SetNextTip(TutorialTexts.DeadEnemy, TutorialTexts.Good);
  }

  public void OnMapModeEntered() {
      SetNextTip(TutorialTexts.MapView, TutorialTexts.Hidden, 0f);
  }
  public void OnHouseClicked() {
      SetNextTip(TutorialTexts.GameplayTip, TutorialTexts.Hidden, 0f);
      StartAsTimedTip(8f);
  }

  void Start()
  {
    textShadow = transform.GetChild(0).gameObject.GetComponent<Text>();
    var tutorialGo = Object.Instantiate(textShadow.gameObject);
    tutorialGo.transform.SetParent(transform);
    tutorialText = tutorialGo.GetComponent<Text>();
    tutorialText.rectTransform.position = textShadow.rectTransform.position;
    tutorialText.rectTransform.localScale = textShadow.rectTransform.localScale;
    textShadow.rectTransform.position -= new Vector3(-10, 10, 0) * 0.5f;
    textShadow.color = Color.blue * 1.2f;
  }

  void Update()
  {
    text = tutorials[ShownTutorials.ContainsKey(currentText)
        ? TutorialTexts.Hidden
        : currentText];
    HandleState();
    PulseText();
  }
  void PulseText()
  {
    var brighness = Mathf.Abs(Mathf.Sin(Time.time * 20f) * 0.1f);
    tutorialText.color = Color.white - new Color(brighness, brighness, brighness, 0f);
  }


  void HandleState()
  {
    if (currentText == TutorialTexts.MovementTip)
    {
      if (Mathf.Abs(Input.GetAxisRaw("Horizontal")) > 0f ||
    Mathf.Abs(Input.GetAxisRaw("Vertical")) > 0f)
        SetNextTip(TutorialTexts.FightTip, TutorialTexts.Good);
      return;
    }

    if (currentText == TutorialTexts.FightTip)
    {
      if (Input.GetButtonDown("Fire1"))
      {
        SetNextTip(TutorialTexts.ObjectiveTip, TutorialTexts.Good);
        // StartAsTimedTip(10f);
      }
      return;
    }

    if (currentText == TutorialTexts.ObjectiveTip)
    {
      // if (timedTipFinished)
      // {
      //   SetNextTip(TutorialTexts.Hidden, TutorialTexts.Hidden, 0f);
      // }
      return;
    }

    if (currentText == TutorialTexts.DeadEnemy)
    {
      // if (timedTipFinished)
      // {
      //   SetNextTip(TutorialTexts.EscKey, TutorialTexts.Hidden, 0f);
      // }
      return;
    }


    if (currentText == TutorialTexts.EscKey)
    {

      // if (timedTipFinished)
      // {
        
      // }
    }

    
    if (currentText == TutorialTexts.GameplayTip)
    {
      if (timedTipFinished)
      {
        SetNextTip(TutorialTexts.Hidden, TutorialTexts.Hidden, 0f);
        tutorials = new Dictionary<TutorialTexts, string>{
        {TutorialTexts.MovementTip, ""},
        {TutorialTexts.Hidden, ""},
        {TutorialTexts.Good, ""},
        {TutorialTexts.FightTip, ""},
        {TutorialTexts.ObjectiveTip, ""},
        {TutorialTexts.DeadEnemy, ""},
        {TutorialTexts.EscKey, ""},
        {TutorialTexts.MapView, ""},
        {TutorialTexts.GameplayTip, ""}
      };
      }
      return;
    }

    // if (timedTipFinished)
    // {
    //   SetNextTip(TutorialTexts.Hidden, TutorialTexts.Hidden, 0f);
    // }
  }


  public void SetNextTip(TutorialTexts next, TutorialTexts delayed = TutorialTexts.Hidden, float delay = 1f)
  {
    currentText = delayed;
    currentDelayTime = delay;
    StartCoroutine("DelayText");
    // ShownTutorials[currentText] = 1;
    nextTutorialText = next;
  }
  void StartAsTimedTip(float delay)
  {
    timedTipDelay = delay;
    StartCoroutine("TimedTip");
  }

  void SetTipImmediate(TutorialTexts next) {
    ShownTutorials[currentText] = 1;
    currentText = next;
    timedTipFinished = true;
  }

  IEnumerator DelayText()
  {
    yield return new WaitForSeconds(currentDelayTime);
    currentText = nextTutorialText;
  }
  IEnumerator TimedTip()
  {
    timedTipFinished = false;
    yield return new WaitForSeconds(timedTipDelay);
    timedTipFinished = true;
  }
}
