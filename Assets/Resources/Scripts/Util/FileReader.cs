﻿using UnityEngine;

public class FileReader
{
    public static MapMetaInfo ReadLevel(string path)
    {
        string[] rawData = ((TextAsset) Resources.Load(Constants.LEVELS_PATH + path)).text.Split('\n');
        MapMetaInfo metaInfo = new MapMetaInfo();
        string json = "";
        for (int j = 0; j < rawData.Length; j++)
        {
            json += rawData[j];
            if (json.Length != 0 && rawData[j].Contains("}"))
            {
                HouseEntity houseEntity = MapMetaInfo.CreateHouseFromJSON(json);
                metaInfo.mapHouses.Add(houseEntity);
                json = "";
            }
        }

        return metaInfo;
    }
}