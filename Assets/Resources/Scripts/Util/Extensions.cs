﻿using System;
using System.Collections.Generic;

namespace Assets.Resources.Scripts.Util
{
    public static class Extensions
    {
        private static Random rnd = new Random();

        public static T PickRandom<T>(this IList<T> coll)
        {
            //first house - uncomment for testing
            //return coll[coll.Count-1];
            return coll[rnd.Next(0, coll.Count - 1)];
        }

        public static void Shuffle<T>(this IList<T> list)
        {
            int n = list.Count;
            while (n > 1)
            {
                n--;
                int k = rnd.Next(n + 1);
                T value = list[k];
                list[k] = list[n];
                list[n] = value;
            }
        }
    }

}