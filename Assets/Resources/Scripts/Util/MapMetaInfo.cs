﻿using System.Collections.Generic;
using UnityEngine;

public class MapMetaInfo
{
    public List<HouseEntity> mapHouses = new List<HouseEntity>();

    public static HouseEntity CreateHouseFromJSON(string jsonString)
    {
        return JsonUtility.FromJson<HouseEntity>(jsonString);
    }
}