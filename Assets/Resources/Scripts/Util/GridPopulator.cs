﻿using System;
using System.Collections.Generic;
using System.Linq;
using Assets.Resources.Scripts.Factories;
using Assets.Resources.Scripts.Map.data;
using UnityEngine;

public class GridPopulator
{
    public static double WIDTH = 2;
    public static double HEIGHT = 2;

    public static float CWIDTH = 30;
    public static float CHEIGHT = 35.5f;
    public static List<HouseEntity> GenerateHouses()
    {
        var ret = new List<HouseEntity>();
        var rects = new List<Rect>();
        foreach (var i in Enumerable.Range(0, 3))
        foreach (var j in Enumerable.Range(0, 3))
            rects.Add(new Rect(i * CWIDTH, j * CHEIGHT, CWIDTH, CHEIGHT));

        foreach (var rect in rects)
        {
            var r = Rect.MinMaxRect((float) (rect.xMin + GridFactory.VROAD_WIDTH / 2), (float) (rect.yMin + GridFactory.ROAD_WIDTH / 2),
                (float) (rect.xMax - GridFactory.VROAD_WIDTH / 2), (float) (rect.yMax - GridFactory.ROAD_WIDTH / 2));
            var cnt = r.center;
            var arr = new[]
            {
                Rect.MinMaxRect(r.xMin, r.yMin, cnt.x, cnt.y),
                Rect.MinMaxRect(r.xMin, cnt.y, cnt.x, r.yMax),
                Rect.MinMaxRect(cnt.x, r.yMin, r.xMax, cnt.y),
                Rect.MinMaxRect(cnt.x, cnt.y, r.xMax, r.yMax)
            };
            Array.ForEach(arr, rr => ret.Add(new HouseEntity((float) (rr.center.x), (float) (rr.center.y + HEIGHT / 2))));
        }
        
        /*foreach (var hori in grid.Horisontals)
        {
            var pv = 0d;
            foreach (var vert in grid.Verticals)
            {
                Rect bounds = new Rect(
                    new Vector2((float) (ph + GridFactory.ROAD_WIDTH / 2), (float) (pv + GridFactory.VROAD_WIDTH / 2)),
                    new Vector2((float) (hori - ph - GridFactory.ROAD_WIDTH),
                        (float) (vert - pv - GridFactory.ROAD_WIDTH)));
                var center = bounds.center;
                var arr = new[]
                {
                    Rect.MinMaxRect(bounds.xMin, bounds.yMin, center.x, center.y),
                    Rect.MinMaxRect(bounds.xMin, center.y, center.x, bounds.yMax),
                    Rect.MinMaxRect(center.x, bounds.yMin, bounds.xMax, center.y),
                    Rect.MinMaxRect(center.x, center.y, bounds.xMax, bounds.yMax),
                };

                Array.ForEach(arr, rect =>
                {
                    ret.Add(new HouseEntity(cnt.x, cnt.y));
                });
                pv = vert;
            }

            ph = hori;
        }*/

        return ret;
    }
}