﻿using UnityEngine;

public class CameraController : MonoBehaviour
{
    public GameObject playerObject = null;
    public float cameraTrackingSpeed = 1f;
    private Vector3 lastTargetPosition = Vector3.zero;
    private Vector3 currTargetPosition = Vector3.zero;
    private float currLerpDistance = 0.0f;
    private float yOffset = 0.1f;
    public float bottomLimit = 0;
    public float leftLimit = 0;
    public float topLimit = 0;
    public float rightLimit = 0;

    private bool inMapMode;

    void Start()
    {
        // Set the initial camera positioning to prevent any weird jerking around
        Vector3 playerPos = playerObject.transform.position;
        Vector3 cameraPos = transform.position;
        Vector3 startTargPos = playerPos;

        // Set the Z to the same as the camera so it does not move
        startTargPos.z = cameraPos.z;
        lastTargetPosition = startTargPos;
        currTargetPosition = startTargPos;
        currLerpDistance = 1.0f;
    }

    void LateUpdate()
    {
        if (inMapMode) return;
        // Update based on our current state
        onStateCycle();
        // Continue moving to the current target position
        currLerpDistance += cameraTrackingSpeed;
        transform.position = Vector3.Lerp(lastTargetPosition, currTargetPosition, currLerpDistance);
        transform.position = new Vector3(Mathf.Clamp(transform.position.x, leftLimit, rightLimit),
            Mathf.Clamp(transform.position.y, bottomLimit, topLimit),
            transform.position.z);
    }

    // Every cycle of the engine, process the current state
    void onStateCycle()
    {
        trackPlayer();
    }

    void trackPlayer()
    {
        // Get and store the current camera position, and the current player position, in world coordinates.
        Vector3 currCamPos = transform.position;
        Vector3 v3 = new Vector3(currCamPos.x, currCamPos.y + yOffset, currCamPos.z);

        Vector3 currPlayerPos = playerObject.transform.position;

        if (currCamPos.x == currPlayerPos.x && currCamPos.y == currPlayerPos.y)
        {
            // Positions are the same - tell the camera not to move, then abort.
            currLerpDistance = 1.0f;
            lastTargetPosition = currCamPos;

            currTargetPosition = currCamPos;
            return;
        }

        // Reset the travel distance for the lerp
        currLerpDistance = 0.0f;

        // Store the current target position so we can lerp from it
        lastTargetPosition = v3;

        // Store the new target position
        currTargetPosition = currPlayerPos;

        // Change the Z position of the target to the same as the current. We don' want that to change.
        currTargetPosition.z = currCamPos.z;
    }

    void stopTrackingPlayer()
    {
        // Set the target positioning to the camera's current position to stop its movement in its tracks
        Vector3 currCamPos = transform.position;
        currTargetPosition = currCamPos;
        lastTargetPosition = currCamPos;

        // Also set the lerp progress distance to 1.0f, which will tell the lerping that it is finished.
        // Since we set the target positionins to the camera's current position, the camera will just
        // lerp to its current spot and stop there.
        currLerpDistance = 1.0f;
    }

    public void ShowAll(bool inMapMode)
    {
        this.inMapMode = inMapMode;
        if (!inMapMode)
        {
            trackPlayer();
            Camera.main.orthographicSize = Constants.CAMERA_DEFAULT_SIZE;
        }
        else
        {
            transform.position = new Vector3(72f, 54f, -10f);
            Camera.main.orthographicSize = Constants.CAMERA_MAP_SIZE;
        }
    }
}