﻿using System.Collections;
using System.Collections.Generic;
using Assets.Resources.Scripts.Util;
using UnityEngine;
using UnityEngine.UI;

public class MapManager : MonoBehaviour
{
    private MapMetaInfo mapMetaInfo;
    private ItemFactory itemFactory;
    private CameraController cameraController;
    private bool inMapMode;
    private PlayerController playerController;
    private HouseEntity playersHouseEntity;
    private List<string> _clues;
    private int FoundCluesCount = 0;
    public GameObject CluesPanel;
    public TutorialTextController TutorialTextController;


    public Text CluesText;
    public Text HomeText;
    private EnemySpawner[] enemySpawners;

    private void Start()
    {
        TutorialTextController = FindObjectOfType<TutorialTextController>();

        playerController = FindObjectOfType<PlayerController>();
        cameraController = FindObjectOfType<CameraController>();
        //AudioSourceManager.instance.Play(AudioResources.AMBIENT_MUSIC);

        itemFactory = gameObject.GetComponent<ItemFactory>();
        itemFactory.Init();
        InitMap();
    }

    private void InitMap()
    {
        //apMetaInfo = FileReader.ReadLevel(Constants.DEFAULT_LEVEL);
        //mapMetaInfo.mapHouses
        var houses = GridPopulator.GenerateHouses();
        houses.Reverse();

        playersHouseEntity = houses.PickRandom();
        playersHouseEntity.IsPlayersHouse = true;
        _clues = playersHouseEntity.GetClues();
        _clues.Shuffle();

        foreach (var houseEntity in houses)
        {
            var house =
                itemFactory.CreateItem(Constants.BUILDING1, houseEntity.positionX, houseEntity.positionY);
            house.GetComponent<BuildingController>().SetHouseProperties(houseEntity);
        }

        enemySpawners = FindObjectsOfType<EnemySpawner>();
        List<int> indexes = new List<int>();
        int count = 3;
        while (true)
        {
            int number = Random.Range(2, 10);
            if (!indexes.Contains(number))
            {
                indexes.Add(number);
                count -= 1;
            }

            if (count <= 0) break;
        }

        foreach (var enemySpawner in enemySpawners) enemySpawner.SpawnEnemies(indexes);
    }

    public EnemySpawner[] getEnemySpawners()
    {
        return enemySpawners;
    }

    public void SwitchMap()
    {
        inMapMode = !inMapMode;
        CluesPanel.gameObject.SetActive(inMapMode);
        cameraController.ShowAll(inMapMode);
        playerController.LockPlayer(inMapMode);

        if (inMapMode)
        {
            TutorialTextController.OnMapModeEntered();
        }
        else
        {
            TutorialTextController.OnHouseClicked();
        }
    }

    public void ShowWrongHouseWarning()
    {
        HomeText.gameObject.SetActive(true);
        StartCoroutine("StartHomeTextRoutine");
    }

    private IEnumerator StartHomeTextRoutine()
    {
        yield return new WaitForSeconds(2f);
        HomeText.gameObject.SetActive(false);
    }

    public void UpdateClues()
    {
        FoundCluesCount += 1;
        var clueText = "FOUND CLUES:" + "\n";
        for (var i = 0; i < FoundCluesCount; i++)
        {
            if (i < _clues.Count)
                clueText += "-" + _clues[i] + "\n";
        }

        AudioSourceManager.instance.Play(AudioResources.PICK_CLUE);
        CluesText.text = "CLUES " + FoundCluesCount + "/" + (Constants.CLUE_COUNT + 1);
        CluesPanel.gameObject.GetComponentInChildren<Text>().text = clueText;
    }

    public bool isInMapMode()
    {
        return inMapMode;
    }

    private void Update()
    {
    }
}