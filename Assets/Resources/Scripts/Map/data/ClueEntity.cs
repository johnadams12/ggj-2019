﻿namespace Assets.Resources.Scripts.Map.data
{
    public class ClueEntity
    {
        public float PositionX;
        public float PositionY;
        public string Text;
        public bool Visible;
        public bool Collected;
    }
}