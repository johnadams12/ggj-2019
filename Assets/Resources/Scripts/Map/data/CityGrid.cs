﻿using System;
using System.Collections.Generic;

namespace Assets.Resources.Scripts.Map.data
{
    /// <summary>
    /// City road grid definitions
    /// </summary>
    public struct CityGrid
    {
        public double[] Verticals;
        public double[] Horisontals;
    }
}