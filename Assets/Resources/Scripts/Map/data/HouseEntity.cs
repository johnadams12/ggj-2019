﻿using System;
using System.Collections.Generic;
using System.Linq;
using Assets.Resources.Scripts.Factories;
using UnityEngine;
using Random = System.Random;

public class HouseEntity
{
    private static Dictionary<int, HashSet<int>> _colors = new Dictionary<int, HashSet<int>>();
    private static Random rnd = new Random();

    public bool IsPlayersHouse = false;

    private static readonly List<Color> ColorList = new List<Color>
    {
        Color.cyan,
        new Color32(255, 127, 0, 1),
        Color.yellow,
        Color.green,
        Color.red,
        Color.gray
    };

    private static readonly List<String> ColorNames = new List<String>
    {
        "Cyan",
        "Orange",
        "Yellow",
        "Green",
        "Red",
        "Gray"
    };

    private static string ColorName(Color c)
    {
        int idx = ColorList.IndexOf(c);
        if (idx < 0)
            return null;
        return ColorNames[idx];
    }

    static HouseEntity()
    {
        foreach (var i in Enumerable.Range(1, 8))
            _colors[i] = new HashSet<int>();
    }

    public int HouseType;
    public bool IsTarget;
    public Color WallColor;
    public Color WindowColor;
    public Color RoofColor;
    public float positionX;
    public float positionY;
    public string name;

    private bool TrySetColor()
    {
        HouseType = rnd.Next(1, 8);
        int i = rnd.Next(ColorList.Count), j = rnd.Next(ColorList.Count), k = rnd.Next(ColorList.Count);
        if (!_colors[HouseType].Add(i * 100 + j * 10 + k))
            return false;
        WallColor = ColorList[i];
        WindowColor = ColorList[j];
        RoofColor = ColorList[k];
        return true;
    }

    public HouseEntity(float positionX, float positionY)
    {
        this.positionX = positionX;
        this.positionY = positionY;
        while (!TrySetColor()) ;
    }

    public List<String> GetClues()
    {
        var ret = new List<String>
        {
            String.Format("{0} walls", ColorName(WallColor)),
            String.Format("{0} windows", ColorName(WindowColor)),
            String.Format("{0} roof", ColorName(RoofColor)),
            String.Format("It is situated in the {0}-{1} part of the map",
                positionY < GridFactory.MAP_HEIGHT / 2 ? "south" : "north",
                positionX < GridFactory.MAP_WIDTH / 2 ? "western" : "eastern"),
           // String.Format("It is of type {0}", HouseType)
        };
        return ret;
    }
}