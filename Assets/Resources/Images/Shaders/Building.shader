﻿// Unity built-in shader source. Copyright (c) 2016 Unity Technologies. MIT license (see license.txt)

Shader "Sprites/Building"
{
    Properties
    {
        [PerRendererData] _MainTex ("Sprite Texture", 2D) = "white" {}
        _Mask ("Mask", 2D) = "white" {}
        _Color ("Tint", Color) = (1,1,1,1)
        _WindowTint ("Window Tint", Color) = (1,1,1,1)
        _WallTint ("Wall Tint", Color) = (1,1,1,1)
        _RoofTint ("Roof Tint", Color) = (1,1,1,1)

        _WindowBrightness ("Window Brightness", Float) = 1.0
        _WallBrightness ("Wall Brightness", Float) = 1.0
        _RoofBrightness ("Roof Brightness", Float) = 1.0

        _Brightness ("Brightness", Float) = 1.0
        _Saturation ("Saturation", Float) = 1.0
        _Contrast ("Contrast", Float) = 1.0


        [MaterialToggle] PixelSnap ("Pixel snap", Float) = 0
        [HideInInspector] _RendererColor ("RendererColor", Color) = (1,1,1,1)
        [HideInInspector] _Flip ("Flip", Vector) = (1,1,1,1)
        [PerRendererData] _AlphaTex ("External Alpha", 2D) = "white" {}
        [PerRendererData] _EnableExternalAlpha ("Enable External Alpha", Float) = 0
    }

    SubShader
    {
        Tags
        {
            "Queue"="Transparent"
            "IgnoreProjector"="True"
            "RenderType"="Transparent"
            "PreviewType"="Plane"
            "CanUseSpriteAtlas"="True"
        }

        Cull Off
        Lighting Off
        ZWrite Off
        Blend One OneMinusSrcAlpha

        Pass
        {
        CGPROGRAM
            #pragma vertex SpriteVert
            #pragma fragment SpriteFragCustom
            #pragma target 2.0
            #pragma multi_compile_instancing
            #pragma multi_compile _ PIXELSNAP_ON
            #pragma multi_compile _ ETC1_EXTERNAL_ALPHA
            #include "UnitySprites.cginc"
            #include "Include/PhotoshopMath.hlsl"

            
            sampler2D _Mask;
            float4 _Mask_ST;
            
            float4 _WindowTint;
            float4 _WallTint;
            float4 _RoofTint;
       float  _WindowBrightness;
        float _WallBrightness;
       float  _RoofBrightness;

       
        float _Brightness;
        float _Saturation;
        float _Contrast;
            
            fixed4 SpriteFragCustom(v2f IN) : SV_Target
            {
                fixed4 c = SampleSpriteTexture (IN.texcoord) * IN.color;
                fixed4 mask = tex2D(_Mask, IN.texcoord);


                c.rgb *= lerp((fixed4)1, _RoofTint * _RoofBrightness , mask.g);
                c.rgb *= lerp((fixed4)1, _WindowTint * _WindowBrightness, mask.b);
                c.rgb *= lerp((fixed4)1, _WallTint * _WallBrightness , mask.r);

                c.rgb = ContrastSaturationBrightness(c.rgb, _Brightness,  _Saturation,  _Contrast);



                c.rgb *= c.a;
                return c;
            }
        ENDCG
        }
    }
}
